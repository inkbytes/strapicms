export default ({ env }) => ({
    upload: {
        config: {
            provider: "strapi-provider-upload-do",
            providerOptions: {
                key: env("DO_SPACE_ACCESS_KEY"),
                secret: env("DO_SPACE_SECRET_KEY"),
                endpoint: env("DO_SPACE_ENDPOINT"),
                space: env("DO_SPACE_BUCKET"),
            },
        },
    },
    webtools:{

        enabled: true,
    },
    'import-export-entries': {
        enabled: true,
        config: {
            // See `Config` section.
            serverPublicHostname: 'https://cms.kloudsix.io',
            xdef:false,
        },
    },
    
});