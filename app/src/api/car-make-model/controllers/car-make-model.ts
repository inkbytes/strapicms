/**
 * car-make-model controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::car-make-model.car-make-model');
