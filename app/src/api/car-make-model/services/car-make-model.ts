/**
 * car-make-model service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::car-make-model.car-make-model');
