/**
 * car-make-model router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::car-make-model.car-make-model');
