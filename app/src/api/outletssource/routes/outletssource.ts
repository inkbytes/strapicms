/**
 * outletssource router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::outletssource.outletssource');
