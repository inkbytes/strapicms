/**
 * car-make service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::car-make.car-make');
