/**
 * car-make controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::car-make.car-make');
