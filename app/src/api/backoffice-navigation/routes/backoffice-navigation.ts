/**
 * backoffice-navigation router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::backoffice-navigation.backoffice-navigation');
