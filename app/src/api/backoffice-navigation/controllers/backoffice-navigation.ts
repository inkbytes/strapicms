/**
 * backoffice-navigation controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::backoffice-navigation.backoffice-navigation');
