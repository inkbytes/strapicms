/**
 * backoffice-navigation service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::backoffice-navigation.backoffice-navigation');
